module.exports = {
  transform: {
    '^.+\\.svelte$': 'jest-transform-svelte',
    '^.+\\.m?js$': 'babel-jest'
  },
  moduleFileExtensions: ['js', 'mjs', 'svelte'],
  testPathIgnorePatterns: ['node_modules', 'cypress'],
  bail: false,
  verbose: true,
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
  setupFiles: ['./jest/setup.js'],
  setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
  moduleDirectories: ['node_modules', 'src/node_modules'],
  modulePaths: ['<rootDir>/node_modules', '<rootDir>/src/node_modules']
};