describe('/browse', () => {
  beforeEach(() => {
    cy.visit('/browse');
  });

  it('shows correct title', () => {
    cy.title().should('include', 'Browse');
  });

  it('displays list of puzzles (10 to start, more on scroll)', () => {
    cy.get('[data-qa="puzzle-card"]').should('have.length', 10);
    cy.get('[data-qa="lazy-load-marker"]').scrollIntoView();
    cy.get('[data-qa="puzzle-card"]').should('have.length.greaterThan', 10);
  });

  it('loads new list of puzzles when dimensions change', () => {
    cy.get('[data-qa="puzzle-card"]').
      first().
      invoke('attr', 'data-id').
      then(dataId => {
        // Click new dimensions
        cy.get('[data-qa="dimensions-radio-input"').first().click();
        cy.get(`[data-id=${dataId}]`).should('not.exist');
      });
  });

  it('navigates to puzzle url when puzzle card is clicked', () => {
    cy.get('[data-qa="puzzle-card"]').
      first().
      invoke('attr', 'data-id').
      then($dataId => {
        cy.get('[data-qa="puzzle-card"]').first().click();
        cy.url().should('include', $dataId);
      });
  });
});