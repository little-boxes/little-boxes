describe('/puzzle/[slug]', () => {
  let puzzleId;

  // Get first puzzle id for navigation
  before(() => {
    cy.visit('/browse');
    cy.get('[data-qa="puzzle-card"]').first().invoke('attr', 'data-id').then(dataId => puzzleId = dataId);
  });

  beforeEach(() => {
    cy.visit(`/puzzle/${puzzleId}`);
  });

  it('puzzle metadata matches between url/title and info panel', () => {
    cy.get('[data-qa="puzzle-id"]').should('include.text', puzzleId);
    cy.get('[data-qa="puzzle-title"]').then($puzzleTitle => {
      cy.title().should('include',  $puzzleTitle.text());
    });
  });
});