const conf = require('config');

const envConfig = {
  video: false,
  defaultCommandTimeout: 10000,
  ...conf.get('cypress'),
};

console.log(`Using config: ${JSON.stringify(envConfig, null, 2)}`);

module.exports = (on, config) => ({
  ...config,
  ...envConfig,
});
