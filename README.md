## Pipelines

### Branches
```mermaid
graph LR
  analyze[Unit Test<br>Browser Test<br>Lint]
```

### Master
```mermaid
graph LR
  build[Build Stage Image]
  build --> deploy
  deploy[Create and Deploy Stage<br>Reset Stage Test Data]
  deploy --> destroy
  destroy[Destroy Stage]
```

### Tags
```mermaid
graph LR
  build[Build Prod Image]
  build --> deploy
  deploy[Deploy Prod]
```
