import {Router} from 'express';
import getDb, {TableName} from '../database';
import {convertPuzzleFromDb} from './helpers/converter';
import {DIMENSIONS} from '../constants';

const router = Router();
const pageSize = 10;

// Get All Puzzles
router.get('/', async (req, res) => {
  try {
    const {
      dimensions = DIMENSIONS.TEN_BY_TEN.VALUE,
      lastPuzzleSeen,
    } = req.query || {};

    const params = {
      TableName,
      KeyConditionExpression: 'dimensions = :dimensions',
      ExpressionAttributeValues: {
        ':dimensions': dimensions,
      },
      Limit: pageSize,
      ScanIndexForward: false,
      IndexName: 'dimensions-likedCt-index',
    };

    if (lastPuzzleSeen) {
      params.ExclusiveStartKey = JSON.parse(lastPuzzleSeen);
    }

    const {Items, LastEvaluatedKey} = await getDb().query(params).promise();

    res.json({
      puzzles: Items.map(convertPuzzleFromDb),
      lastPuzzleSeen: LastEvaluatedKey,
    });
  } catch (err) {
    res.status(err.statusCode || 503).send('Failed to retrieve puzzles from DB');
  }
});

// Get Single Puzzle
router.get('/:puzzleId', async (req, res, next) => {
  try {
    const {Item} = await getDb().get({
      TableName,
      Key: {
        pk: `puzzle_${req.params.puzzleId}`,
        sk: 'details',
      }
    }).promise();

    if (Item) {
      const appPuzzle = convertPuzzleFromDb(Item);

      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(appPuzzle));
    } else {
      next();
    }
  } catch (err) {
    res.status(err.statusCode || 503).send('Failed to retrieve puzzle from DB');
  }
});

export default router;
