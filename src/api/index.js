import {Router} from 'express';
import puzzle from './puzzle';

const router = Router();

router.use('/puzzle', puzzle);

export default router;
