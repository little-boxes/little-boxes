import express from 'express';
import AWS from 'aws-sdk-mock';
import request from 'supertest';
import puzzleRoute from './puzzle.js';
import {defaultDimensions} from '../routes/browse/_store/dimensions.js';

jest.mock('./helpers/converter');

const app = express().use('/', puzzleRoute);

afterEach(() => {
  AWS.restore('DynamoDB.DocumentClient');
});

const getError = (statusCode) => {
  const err = new Error('DB Exception');

  if (statusCode) {
    err.statusCode = statusCode;
  }
  return err;
};

describe('/puzzle', () => {
  describe('/', () => {
    // Database mocking
    const Items = ['a', 'b', 'c'];
    const dbMock = jest.fn().mockImplementation((params, cb) => cb(null, {Items}));

    beforeEach(() => {
      dbMock.mockClear();
      AWS.mock('DynamoDB.DocumentClient', 'query', dbMock);
    });

    it('uses default dimensions if no query', async () => {
      const res = await request(app).get('/');

      expect(res.status).toEqual(200);
      expect(res.body.puzzles).toEqual(Items);
      expect(dbMock.mock.calls[0][0]).
        toHaveProperty('ExpressionAttributeValues.:dimensions', defaultDimensions);
    });

    it('uses query dimensions if provided', async () => {
      const dimensions = '5x5';
      const res = await request(app).get('/').query({dimensions});

      expect(res.status).toEqual(200);
      expect(res.body.puzzles).toEqual(Items);
      expect(dbMock.mock.calls[0][0]).
        toHaveProperty('ExpressionAttributeValues.:dimensions', dimensions);
    });

    it('uses query lastPuzzleSeen to determine page start', async () => {
      const lastPuzzleSeen = {id: 'some_puzzle_id'};
      const res = await request(app).get('/').query({lastPuzzleSeen: JSON.stringify(lastPuzzleSeen)});

      expect(res.status).toEqual(200);
      expect(res.body.puzzles).toEqual(Items);
      expect(dbMock.mock.calls[0][0]).toHaveProperty('ExclusiveStartKey', lastPuzzleSeen);
    });

    it('returns err.statusCode on caught error', async () => {
      AWS.restore('DynamoDB.DocumentClient');
      const dbMock = jest.fn().mockImplementation((params, cb) => cb(getError(400)));

      AWS.mock('DynamoDB.DocumentClient', 'query', dbMock);
      const res = await request(app).get('/');

      expect(res.status).toEqual(400);
    });

    it('returns 503 (network timeout) if no statusCode on caught error', async () => {
      AWS.restore('DynamoDB.DocumentClient');
      const dbMock = jest.fn().mockImplementation((params, cb) => cb(getError()));

      AWS.mock('DynamoDB.DocumentClient', 'query', dbMock);
      const res = await request(app).get('/');

      expect(res.status).toEqual(503);
    });
  });

  describe('/:puzzleId', () => {
    it('returns puzzle if it exists', async () => {
      const puzzleId = '123';
      const Item = {id: puzzleId};
      const dbMock = jest.fn().mockImplementation((params, cb) => cb(null, {Item}));

      AWS.mock('DynamoDB.DocumentClient', 'get', dbMock);
      const res = await request(app).get(`/${puzzleId}`);

      expect(res.status).toEqual(200);
      expect(res.body).toEqual(Item);
      expect(dbMock.mock.calls[0][0]).
        toHaveProperty('Key.pk', `puzzle_${puzzleId}`);
    });

    it('returns 404 if puzzle doesnt exist', async () => {
      const dbMock = jest.fn().mockImplementation((params, cb) => cb(null, {}));

      AWS.mock('DynamoDB.DocumentClient', 'get', dbMock);
      const res = await request(app).get('/bad_puzzle_id');

      expect(res.status).toEqual(404);
    });


    it('returns err.statusCode on caught error', async () => {
      const dbMock = jest.fn().mockImplementation((params, cb) => cb(getError(400)));

      AWS.mock('DynamoDB.DocumentClient', 'query', dbMock);
      const res = await request(app).get('/');

      expect(res.status).toEqual(400);
    });

    it('returns 503 (network timeout) if no statusCode on caught error', async () => {
      const dbMock = jest.fn().mockImplementation((params, cb) => cb(getError()));

      AWS.mock('DynamoDB.DocumentClient', 'query', dbMock);
      const res = await request(app).get('/');

      expect(res.status).toEqual(503);
    });
  });
});
