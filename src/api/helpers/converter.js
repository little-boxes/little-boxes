export function convertPuzzleFromDb(dbPuzzle, options = {}) {
  const {
    includeArtistEmail = false,
    includeSolution = false,
  } = options;

  const {
    pk,
    sk,
    artistEmail,
    solution,
    ...appPuzzle
  } = dbPuzzle;
  const [width, height] = dbPuzzle.dimensions.split('x').map(n => parseInt(n));

  if (includeArtistEmail) {
    appPuzzle.artistEmail = artistEmail;
  }
  if (includeSolution) {
    appPuzzle.solution = solution;
  }

  appPuzzle.puzzleId = pk.replace('puzzle_', '');
  appPuzzle.dimensions = {width, height};
  return appPuzzle;
}
