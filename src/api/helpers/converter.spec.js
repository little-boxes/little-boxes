import {convertPuzzleFromDb} from './converter';

const dbPuzzle = {
  pk: 'puzzle_1',
  sk: 'details',
  title: 'Bloop Bloop',
  likedCt: 5,
  dimensions: '10x10',
  clues: {
    rows: [
      [3, 2],
      [3],
      [2, 2],
      [4],
      [4],
      [2, 2],
      [4, 1],
      [4, 2],
      [2, 2],
      [1]
    ],
    cols: [
      [3],
      [3, 2],
      [2, 4],
      [4],
      [2],
      [2],
      [4],
      [1, 4],
      [1, 2, 2],
      [4]
    ]
  },
  solution: '1110000110111000000011000011000000011110000001111000110011000111100001011110001100110000110000000001',
  s3Thumbnail: 's3://puzzle_1.jpg',
  coloringCt: 2,
  solvedCt: 1,
  artistDisplayName: 'Casey Van Groll',
  artistEmail: 'casey@gmail.com'
};

const appPuzzle = {
  puzzleId: '1',
  title: 'Bloop Bloop',
  dimensions: {
    width: 10,
    height: 10,
  },
  clues: {
    rows: [
      [3, 2],
      [3],
      [2, 2],
      [4],
      [4],
      [2, 2],
      [4, 1],
      [4, 2],
      [2, 2],
      [1]
    ],
    cols: [
      [3],
      [3, 2],
      [2, 4],
      [4],
      [2],
      [2],
      [4],
      [1, 4],
      [1, 2, 2],
      [4]
    ]
  },
  solution: '1110000110111000000011000011000000011110000001111000110011000111100001011110001100110000110000000001',
  s3Thumbnail: 's3://puzzle_1.jpg',
  coloringCt: 2,
  solvedCt: 1,
  artistDisplayName: 'Casey Van Groll',
  artistEmail: 'casey@gmail.com'
};

describe('api helpers - converter', () => {
  test('should filter solution unless includeSolution is true', () => {
    expect(convertPuzzleFromDb(dbPuzzle).solution).toBeUndefined();
    expect(convertPuzzleFromDb(dbPuzzle, {includeSolution: true}).solution).toEqual(appPuzzle.solution);
  });
  test('should filter artistEmail unless includeArtistEmail is true', () => {
    expect(convertPuzzleFromDb(dbPuzzle).artistEmail).toBeUndefined();
    expect(convertPuzzleFromDb(dbPuzzle, {includeArtistEmail: true}).artistEmail).toEqual(appPuzzle.artistEmail);
  });
  test('should convert dimensions from string to object', () => {
    expect(convertPuzzleFromDb(dbPuzzle).dimensions).toEqual(appPuzzle.dimensions);
  });
  test('should filter pk and sk', () => {
    expect(convertPuzzleFromDb(dbPuzzle).pk).toBeUndefined();
    expect(convertPuzzleFromDb(dbPuzzle).sk).toBeUndefined();
  });
  test('should derive puzzleId from pk', () => {
    expect(convertPuzzleFromDb(dbPuzzle).puzzleId).toEqual(dbPuzzle.pk.replace('puzzle_', ''));
  });
});
