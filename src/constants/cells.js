export const FILLED = '#000';
export const EMPTY = '#FFF';
export const CROSSED = '-';

export default {
  FILLED,
  EMPTY,
  CROSSED,
};
