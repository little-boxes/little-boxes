const FIVE_BY_FIVE = {
  VALUE: '5x5',
  height: 5,
  width: 5,
};

const TEN_BY_TEN = {
  VALUE: '10x10',
  height: 10,
  width: 10,
};

const FIFTEEN_BY_FIFTEEN = {
  VALUE: '15x15',
  height: 15,
  width: 15,
};

const TWENTY_BY_TWENTY = {
  VALUE: '20x20',
  height: 20,
  width: 20,
};

export default {
  ALL: [
    FIVE_BY_FIVE,
    TEN_BY_TEN,
    FIFTEEN_BY_FIFTEEN,
    TWENTY_BY_TWENTY,
  ],
  FIVE_BY_FIVE,
  TEN_BY_TEN,
  FIFTEEN_BY_FIFTEEN,
  TWENTY_BY_TWENTY,
};
