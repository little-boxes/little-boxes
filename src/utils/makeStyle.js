// Need common.js export or svelte will complain
import toStyle from 'to-style';

export default style => toStyle.string(style, {addUnits: false});
