import {FILLED} from '../constants/cells.js';

const getCellTypeFromMouseEvent = event => {
  if (event.buttons === 1) {
    return FILLED;
  }
};

export default getCellTypeFromMouseEvent;
