import makeInitialRows from './makeInitialRows.js';
import {FILLED, EMPTY, CROSSED} from '../constants/cells.js';

describe('makeInitialRows', () => {
  it('(default) makes 2d array of empty cells', () => {
    const rows = makeInitialRows({height: 3, width: 3});

    expect(rows).toEqual([
      [EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY],
    ]);
  });

  it('makes 2d array of cells with previous values (change to smaller dimensions)', () => {
    const prevRows = [
      [FILLED, CROSSED, FILLED],
      [FILLED, CROSSED, FILLED],
      [FILLED, CROSSED, FILLED],
    ];
    const rows = makeInitialRows({height: 2, width: 2}, prevRows);

    expect(rows).toEqual([
      [FILLED, CROSSED],
      [FILLED, CROSSED],
    ]);
  });

  it('makes 2d array of cells with previous values and empties (change to larger dimensions)', () => {
    const prevRows = [
      [FILLED, CROSSED],
      [FILLED, CROSSED],
    ];
    const rows = makeInitialRows({height: 3, width: 3}, prevRows);

    expect(rows).toEqual([
      [FILLED, CROSSED, EMPTY],
      [FILLED, CROSSED, EMPTY],
      [EMPTY, EMPTY, EMPTY],
    ]);
  });
});
