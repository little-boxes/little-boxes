import {fly} from 'svelte/transition';
import {quintIn, quintOut} from 'svelte/easing';

export function flyIn(el) {
  const {width} = el.getBoundingClientRect();

  return fly(el, {x: window.innerWidth + width, easing: quintIn, duration: 400});
}

export function flyOut(el) {
  const {width} = el.getBoundingClientRect();

  return fly(el, {x: -width, easing: quintOut, duration: 400});
}
