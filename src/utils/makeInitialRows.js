import {CELLS} from '../constants';

const makeInitialRows = (dimensions, prevValues) => {
  const {height, width} = dimensions;
  const rows = new Array(height).fill().map(() => new Array(width).fill());

  const getPrevValue = (row, col) => {
    if (prevValues[row]) {
      return prevValues[row][col] || CELLS.EMPTY;
    }
    return CELLS.EMPTY;
  };

  for (let row = 0; row < height; row += 1) {
    for (let col = 0; col < width; col += 1) {
      rows[row][col] = prevValues ? getPrevValue(row, col) : CELLS.EMPTY;
    }
  }

  return rows;
};

export default makeInitialRows;
