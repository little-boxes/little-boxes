import getCellsStore from './getCellsStore.js';
import * as makeInitialRows from './makeInitialRows.js';
import {FILLED, EMPTY, CROSSED} from '../constants/cells.js';

describe('rows, cols, dimensions [store]', () => {
  let rows, $rows, $cols, $dimensions, setCell, updateDimensions, unsubscribes;

  beforeEach(() => {
    const cellsStore = getCellsStore();

    unsubscribes = [
      cellsStore.rows.subscribe(val => $rows = val),
      cellsStore.cols.subscribe(val => $cols = val),
      cellsStore.dimensions.subscribe(val => $dimensions = val),
    ];

    rows = cellsStore.rows;
    setCell = cellsStore.setCell;
    updateDimensions = cellsStore.updateDimensions;
  });

  afterEach(() => {
    unsubscribes.forEach(unsubscribe => unsubscribe());
  });

  it('uses 10x10 for default dimensions', () => {
    expect($rows).toEqual([
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY],
    ]);
    expect($dimensions).toEqual({
      width: 10,
      height: 10,
      VALUE: '10x10'
    });
  });

  it('cols and dimensions are derived correctly', () => {
    rows.set([
      [EMPTY, FILLED, CROSSED],
      [EMPTY, EMPTY, FILLED],
    ]);
    expect($cols).toEqual([
      [EMPTY, EMPTY],
      [FILLED, EMPTY],
      [CROSSED, FILLED],
    ]);
    expect($dimensions).toEqual({
      width: 3,
      height: 2,
      VALUE: '3x2'
    });
  });

  it('setCell sets cell at correct location', () => {
    rows.set([
      [EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY],
    ]);
    setCell(1, 1, FILLED);
    setCell(1, 2, CROSSED);
    expect($rows).toEqual([
      [EMPTY, EMPTY, EMPTY],
      [EMPTY, FILLED, CROSSED],
    ]);
    expect($cols).toEqual([
      [EMPTY, EMPTY],
      [EMPTY, FILLED],
      [EMPTY, CROSSED],
    ]);
  });

  it('updateDimensions only passes rows if asked to retain values', () => {
    const makeInitialRowsSpy = jest.spyOn(makeInitialRows, 'default');
    const dimensions = {height: 2, width: 2};

    updateDimensions(dimensions);
    expect(makeInitialRowsSpy).toHaveBeenCalledWith(dimensions, undefined);

    updateDimensions(dimensions, true);
    expect(makeInitialRowsSpy).toHaveBeenCalledWith(dimensions, $rows);
  });

});
