import {writable, derived} from 'svelte/store';
import {writable as persistentWritable} from 'svelte-persistent-store/dist/local';
import {DIMENSIONS} from '../constants';
import makeInitialRows from './makeInitialRows.js';

function getCellsStore(storageKey) {
  const initialRows = makeInitialRows(DIMENSIONS.TEN_BY_TEN);

  const rows = storageKey ?
    persistentWritable(storageKey, initialRows) :
    writable(initialRows);

  const cols = derived(rows, $rows => {
    const $cols = new Array($rows[0].length).fill().map(() => new Array($rows.length).fill());

    for (let row = 0; row < $rows.length; row += 1) {
      for (let col = 0; col < $rows[0].length; col += 1) {
        $cols[col][row] = $rows[row][col];
      }
    };

    return $cols;
  });

  const dimensions = derived(rows, $rows => ({
    height: $rows.length,
    width: $rows[0].length,
    VALUE: `${$rows[0].length}x${$rows.length}`,
  }));

  const setCell = (rowIndex, colIndex, value) => {
    rows.update($rows =>
      $rows.map((row, y) =>
        y !== rowIndex ?
          row :
          row.map((col, x) =>
            x !== colIndex ?
              col :
              value
          )
      )
    );
  };

  const updateDimensions = (nextDimensions, retainValues) => {
    rows.update($rows => makeInitialRows(nextDimensions, retainValues && $rows));
  };

  return {
    rows,
    cols,
    dimensions,
    setCell,
    updateDimensions,
  };
}

export default getCellsStore;
