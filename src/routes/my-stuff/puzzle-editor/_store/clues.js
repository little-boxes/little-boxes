import {derived} from 'svelte/store';
import {writable} from 'svelte-persistent-store/dist/local';
import {rows, cols} from './cells.js';

export const showClues = writable('puzzle_editor_show_clues', false);

const getClues = seq => (seq.match(/((?:#000)+)/g) || []).map(match => match.length / 4);

const clues = derived([rows, cols], ([$rows, $cols]) => ({
  rows: $rows.map(row => getClues(row.join(''))),
  cols: $cols.map(col => getClues(col.join(''))),
}));

export default clues;
