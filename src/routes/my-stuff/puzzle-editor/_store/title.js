import {writable} from 'svelte-persistent-store/dist/local';

const title = writable('puzzle_editor_title', '');

export default title;
