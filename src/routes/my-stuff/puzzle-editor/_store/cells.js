import {derived} from 'svelte/store';
import {CELLS} from '../../../../constants';
import getCellsStore from '../../../../utils/getCellsStore.js';
import getCellTypeFromMouseEvent from '../../../../utils/getCellTypeFromMouseEvent.js';
import runInBrowserOnly from '../../../../utils/runInBrowserOnly.js';

const cellsStore = getCellsStore('puzzle_editor_cells');

export const rows = cellsStore.rows;
export const cols = cellsStore.cols;
export const dimensions = cellsStore.dimensions;
export const setCell = cellsStore.setCell;
export const updateDimensions = cellsStore.updateDimensions;

let activeCellType, isErase;

const handleMouseUp = () => {
  activeCellType = undefined;
  isErase = undefined;
};

export const handleMouseDown = derived(rows, $rows => {
  return (row, col, event) => {
    // When multiple buttons are pressed use the first one
    if (!activeCellType) {
      const nextActiveCellType = getCellTypeFromMouseEvent(event);

      // Don't overwrite
      if (nextActiveCellType && [nextActiveCellType, CELLS.EMPTY].includes($rows[row][col])) {
        const nextIsErase = $rows[row][col] === nextActiveCellType;
        const nextCellType = nextIsErase ? CELLS.EMPTY : nextActiveCellType;

        if ($rows[row][col] !== nextCellType) {
          setCell(row, col, nextCellType);
          activeCellType = nextActiveCellType;
          isErase = nextIsErase;
          window.addEventListener('mouseup', handleMouseUp);
        }
      }
    }
  };
});

export const handleMouseEnter = derived(rows, $rows => {
  return (row, col) => {
    if (activeCellType && [activeCellType, CELLS.EMPTY].includes($rows[row][col])) {
      const nextCellType = isErase ? CELLS.EMPTY : activeCellType;

      if ($rows[row][col] !== nextCellType) {
        setCell(row, col, nextCellType);
      }
    }
  };
});

export const removeMouseUpListener = () => {
  runInBrowserOnly(() => window.removeEventListener('mouseup', handleMouseUp));
};
