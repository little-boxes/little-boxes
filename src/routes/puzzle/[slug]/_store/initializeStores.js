import {updateDimensions} from './cells.js';
import clues from './clues.js';

function initializeStores(puzzle) {
  updateDimensions(puzzle.dimensions);
  clues.set(puzzle.clues);
}

export default initializeStores;
