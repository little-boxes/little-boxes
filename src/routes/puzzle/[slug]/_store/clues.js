import {writable} from 'svelte/store';

const clues = writable({
  rows: [],
  cols: [],
});

export default clues;
