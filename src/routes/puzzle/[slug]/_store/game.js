import {derived} from 'svelte/store';
import clues from './clues.js';
import {rows, cols} from './cells.js';
import areCluesSatisfiable from '../_utils/areCluesSatisfiable.js';
import areCluesCompleted from '../_utils/areCluesCompleted.js';

// --------- Row/Col Status ---------

// Constants
export const SATISFIABLE = 'SATISFIABLE';
export const UNSATISFIABLE = 'UNSATISFIABLE';
export const COMPLETED = 'COMPLETED';

// Store
export const rowStatus = derived([clues, rows], ([$clues, $rows]) => {
  return $clues.rows.map((rowClues, rowIndex) => {
    const row = $rows[rowIndex];

    if (areCluesCompleted(rowClues, row)) {
      return COMPLETED;
    }
    if (areCluesSatisfiable(rowClues, row)) {
      return SATISFIABLE;
    }

    return UNSATISFIABLE;
  });
});

// Store
export const colStatus = derived([clues, cols], ([$clues, $cols]) => {
  return $clues.cols.map((colClues, colIndex) => {
    const col = $cols[colIndex];

    if (areCluesCompleted(colClues, col)) {
      return COMPLETED;
    }
    if (areCluesSatisfiable(colClues, col)) {
      return SATISFIABLE;
    }

    return UNSATISFIABLE;
  });
});

// --------- Game Status ---------

// Constants
export const IN_PROGRESS = 'IN_PROGRESS';
export const WIN = 'WIN';

// Store
export const gameStatus = derived([rowStatus, colStatus], ([$rowStatus, $colStatus]) => {
  if (
    $rowStatus.length && $rowStatus.every(status => status === COMPLETED) &&
    $colStatus.length && $colStatus.every(status => status === COMPLETED)
  ) {
    return WIN;
  }

  return IN_PROGRESS;
});
