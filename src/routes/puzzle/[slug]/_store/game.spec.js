import {rows, updateDimensions} from './cells.js';
import clues from './clues.js';
import {
  UNSATISFIABLE,
  SATISFIABLE,
  COMPLETED,
  IN_PROGRESS,
  WIN,
  rowStatus,
  colStatus,
  gameStatus
} from './game.js';
import {FILLED, EMPTY, CROSSED} from '../../../../constants/cells.js';

const convertFromShorthand = rowVals => {
  return rowVals.map(row =>
    row.split('').map(c => {
      switch (c) {
      case 'X': return FILLED;
      case '-': return CROSSED;
      default: return EMPTY;
      }
    })
  );
};

describe('game [store]', () => {
  let unsubscribe, $gameStatus, $rowStatus, $colStatus;

  beforeEach(() => {
    const unsubs = [
      gameStatus.subscribe(val => $gameStatus = val),
      rowStatus.subscribe(val => $rowStatus = val),
      colStatus.subscribe(val => $colStatus = val),
    ];

    unsubscribe = () => unsubs.forEach(unsub => unsub());
  });
  afterEach(() => unsubscribe());

  describe('1x1', () => {
    beforeEach(() => {
      updateDimensions({height: 1, width: 1});
      clues.set({rows: [[1]],cols: [[1]]});
    });

    it.each([
      [
        [' '],
        {
          gameStatus: IN_PROGRESS,
          rowStatus: [SATISFIABLE],
          colStatus: [SATISFIABLE],
        }
      ], [
        ['-'],
        {
          gameStatus: IN_PROGRESS,
          rowStatus: [UNSATISFIABLE],
          colStatus: [UNSATISFIABLE],
        }
      ], [
        ['X'],
        {
          gameStatus: WIN,
          rowStatus: [COMPLETED],
          colStatus: [COMPLETED],
        }
      ]
    ])('returns correct for %s', (rowVals, expected) => {
      rows.set(convertFromShorthand(rowVals));
      expect($gameStatus).toEqual(expected.gameStatus);
      expect($rowStatus).toEqual(expected.rowStatus);
      expect($colStatus).toEqual(expected.colStatus);
    });
  });

  describe('2x3', () => {
    beforeEach(() => {
      updateDimensions({height: 2, width: 3});
      clues.set({
        rows: [[2], [1, 1]],
        cols: [[2], [1], [1]],
      });
    });

    it.each([
      // [
      //   [
      //     '   ',
      //     '   ',
      //   ],
      //   {
      //     gameStatus: IN_PROGRESS,
      //     rowStatus: [SATISFIABLE, SATISFIABLE],
      //     colStatus: [SATISFIABLE, SATISFIABLE, SATISFIABLE],
      //   }
      // ], [
      //   [
      //     'X  ',
      //     'X X',
      //   ],
      //   {
      //     gameStatus: IN_PROGRESS,
      //     rowStatus: [SATISFIABLE, COMPLETED],
      //     colStatus: [COMPLETED, SATISFIABLE, COMPLETED],
      //   }
      // ], [
      //   [
      //     'XX ',
      //     'X--',
      //   ],
      //   {
      //     gameStatus: IN_PROGRESS,
      //     rowStatus: [COMPLETED, UNSATISFIABLE],
      //     colStatus: [COMPLETED, COMPLETED, SATISFIABLE],
      //   }
      // ], [
      //   [
      //     'XX-',
      //     'X-X',
      //   ],
      //   {
      //     gameStatus: WIN,
      //     rowStatus: [COMPLETED, COMPLETED],
      //     colStatus: [COMPLETED, COMPLETED, COMPLETED],
      //   }
      // ],
      [
        [
          'XX ',
          'X X',
        ],
        {
          gameStatus: WIN,
          rowStatus: [COMPLETED, COMPLETED],
          colStatus: [COMPLETED, COMPLETED, COMPLETED],
        }
      ]
    ])('returns correct for %s', (rowVals, expected) => {
      rows.set(convertFromShorthand(rowVals));
      expect($gameStatus).toEqual(expected.gameStatus);
      expect($rowStatus).toEqual(expected.rowStatus);
      expect($colStatus).toEqual(expected.colStatus);
    });
  });
});
