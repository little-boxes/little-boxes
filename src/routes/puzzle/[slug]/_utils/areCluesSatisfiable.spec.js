import areCluesSatisfiable from './areCluesSatisfiable.js';
import {FILLED, EMPTY, CROSSED} from '../../../../constants/cells.js';

const convertFromShorthand = s => {
  return s.split('').map(c => {
    switch (c) {
    case 'X': return FILLED;
    case '-': return CROSSED;
    default: return EMPTY;
    }
  });
};

describe('areCluesSatisfiable', () => {
  it.each([
    // repeats of areCluesSatisfied
    [[1], ' ', true],
    [[1], 'X', true],
    [[1], ' X', true],
    [[1], 'XX', false],
    [[1, 1], 'XX', false],
    [[3], 'XX-', false],
    [[3], 'XX-X', false],
    [[3], ' XXX ', true],
    [[1, 1], 'X    --    X', true],
    // special cases
    [[3], '  ', false],
    [[3], '   ', true],
    [[3], ' - ', false],
    [[3, 3], '   -   ', true],
    [[1, 1, 1], '- - -', false],
    [[1, 2], '  - ', false],
    [[1, 2], '  -  ', true],
  ])('areCluesSatisfiable(%s, "%s") = %s', (clues, cells, expected) => {
    expect(areCluesSatisfiable(clues, convertFromShorthand(cells))).toEqual(expected);
  });
});