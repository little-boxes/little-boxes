import {FILLED, EMPTY, CROSSED} from '../../../../constants/cells.js';

const areCluesSatisfiable = (clues, cells) => {
  const clueGroups = clues.map(num => `(${EMPTY}|${FILLED})\{${num}\}`);
  const areCluesSatisfiableRegex = new RegExp(
    `^(${EMPTY}|${CROSSED})*${clueGroups.join(`(${EMPTY}|${CROSSED})+`)}(${EMPTY}|${CROSSED})*$`
  );

  return areCluesSatisfiableRegex.test(cells.join(''));
};

export default areCluesSatisfiable;
