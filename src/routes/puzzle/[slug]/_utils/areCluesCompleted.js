import {FILLED, EMPTY, CROSSED} from '../../../../constants/cells.js';

const areCluesCompleted = (clues, cells) => {
  const clueGroups = clues.map(num => `(${FILLED})\{${num}\}`);
  const areCluesCompletedRegex = new RegExp(
    `^(${EMPTY}|${CROSSED})*${clueGroups.join(`(${EMPTY}|${CROSSED})+`)}(${EMPTY}|${CROSSED})*$`
  );

  return areCluesCompletedRegex.test(cells.join(''));
};

export default areCluesCompleted;
