import areCluesCompleted from './areCluesCompleted.js';
import {FILLED, EMPTY, CROSSED} from '../../../../constants/cells.js';

const convertFromShorthand = s => {
  return s.split('').map(c => {
    switch (c) {
    case 'X': return FILLED;
    case '-': return CROSSED;
    default: return EMPTY;
    }
  });
};

describe('areCluesCompleted', () => {
  it.each([
    [[1], ' ', false],
    [[1], 'X', true],
    [[1], ' X', true],
    [[1], 'XX', false],
    [[1, 1], 'XX', false],
    [[3], 'XX-', false],
    [[3], 'XX-X', false],
    [[3], ' XXX ', true],
    [[1, 1], 'X    --    X', true],
  ])('isComplete(%s, "%s") = %s', (clues, cells, expected) => {
    expect(areCluesCompleted(clues, convertFromShorthand(cells))).toEqual(expected);
  });
});