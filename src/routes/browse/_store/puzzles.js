import qs from 'query-string';
import {get as currentValueOf, writable} from 'svelte/store';
import {get} from '../../../fetch';
import lastPuzzleSeenStore from './lastPuzzleSeen';
import dimensions, {defaultDimensions} from './dimensions';

export const isFetchingPuzzles = writable(false);
const puzzles = writable([]);

export async function fetchPuzzles(dimensionsToFetch = defaultDimensions, lastPuzzleSeen, dimensionsHaveChanged) {
  isFetchingPuzzles.set(true);
  lastPuzzleSeenStore.set(null);
  if (dimensionsHaveChanged) {
    puzzles.set([]);
  }
  let fetchedPuzzles = [];

  try {
    const query = qs.stringify({
      dimensions: dimensionsToFetch,
      lastPuzzleSeen: JSON.stringify(lastPuzzleSeen)
    }, {skipNull: true});
    const res = await get(`/api/puzzle?${query}`);

    if (!res.ok) {
      throw new Error('Unable to fetch puzzles');
    }

    const resBody = await res.json();

    lastPuzzleSeenStore.set(resBody.lastPuzzleSeen);
    fetchedPuzzles = resBody.puzzles;
  } catch (e) {
    fetchedPuzzles = [];
  }

  // If dimensions store changed there's already another fetch running, don't interfere
  if (dimensionsToFetch === currentValueOf(dimensions)) {
    puzzles.update(prevPuzzles => {
      return [...prevPuzzles, ...fetchedPuzzles];
    });
    isFetchingPuzzles.set(false);
  }
};

export default puzzles;
