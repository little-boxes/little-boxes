import {get} from 'svelte/store';
import qs from 'query-string';
import puzzles, {fetchPuzzles, isFetchingPuzzles} from './puzzles.js';
import lastPuzzleSeen from './lastPuzzleSeen.js';
import {defaultDimensions} from '../../../constants/dimensions.js';

const response = {
  puzzles: [1, 2],
  lastPuzzleSeen: 1,
};

describe('puzzles [store]', () => {
  beforeEach(() => {
    fetch.resetMocks();
    fetch.mockResponse(JSON.stringify(response));
    puzzles.set([]);
    lastPuzzleSeen.set(null);
    isFetchingPuzzles.set(false);
  });

  it('initializes with empty array', async () => {
    expect(get(puzzles)).toEqual([]);
  });

  it('uses default dimensions if none provided', async () => {
    await fetchPuzzles();

    const expectedUri = expect.stringContaining(qs.stringify({
      dimensions: defaultDimensions,
    }));

    expect(fetch).toHaveBeenLastCalledWith(expectedUri, expect.anything());
  });

  it('uses dimensions if provided', async () => {
    await fetchPuzzles('5x5');

    const expectedUri = expect.stringContaining(qs.stringify({
      dimensions: '5x5',
    }));

    expect(fetch).toHaveBeenLastCalledWith(expectedUri, expect.anything());
  });

  it('uses lastPuzzleSeen if provided', async () => {
    await fetchPuzzles(defaultDimensions, 1);

    const expectedUri = expect.stringContaining(qs.stringify({
      dimensions: defaultDimensions,
      lastPuzzleSeen: 1,
    }));

    expect(fetch).toHaveBeenLastCalledWith(expectedUri, expect.anything());
  });

  it('appends to puzzles if dimensions havent changed', async () => {
    await fetchPuzzles();
    expect(get(puzzles)).toEqual(response.puzzles);
    await fetchPuzzles();
    expect(get(puzzles)).toEqual([...response.puzzles, ...response.puzzles]);
  });

  it('clears and sets puzzles if dimensions have changed', async () => {
    await fetchPuzzles();
    expect(get(puzzles)).toEqual(response.puzzles);

    const secondResponse = {
      puzzles: [3, 4],
      lastPuzzleSeen: 3,
    };

    fetch.mockResponse(JSON.stringify(secondResponse));
    await fetchPuzzles(defaultDimensions, null, true);
    expect(get(puzzles)).toEqual(secondResponse.puzzles);
  });

  it('sets last puzzle seen from response', async () => {
    expect(get(lastPuzzleSeen)).toEqual(null);
    await fetchPuzzles();
    expect(get(lastPuzzleSeen)).toEqual(response.lastPuzzleSeen);
  });


  it('resolves stores to correct values on dimensions-changed failure', async () => {
    const originalError = console.error;

    console.error = jest.fn();
    fetch.resetMocks();
    fetch.mockReject(new Error('oh no!'));

    puzzles.set([1, 2]);
    await fetchPuzzles();
    expect(get(isFetchingPuzzles)).toEqual(false);
    expect(get(lastPuzzleSeen)).toEqual(null);
    expect(get(puzzles)).toEqual([1, 2]);

    console.error = originalError;
  });

  it('resolves stores to correct values on dimensions-same failure', async () => {
    const originalError = console.error;

    console.error = jest.fn();
    fetch.resetMocks();
    fetch.mockReject(new Error('oh no!'));

    puzzles.set([1, 2]);
    await fetchPuzzles(defaultDimensions, null, true);
    expect(get(isFetchingPuzzles)).toEqual(false);
    expect(get(lastPuzzleSeen)).toEqual(null);
    expect(get(puzzles)).toEqual([]);

    console.error = originalError;
  });
});
