import {writable} from 'svelte/store';

// Indicates that there are more puzzles to be fetched for this dimension size
const lastPuzzleSeen = writable(null);

export default lastPuzzleSeen;
