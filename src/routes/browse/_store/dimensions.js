import {writable} from 'svelte/store';
import {DIMENSIONS} from '../../../constants';

export const defaultDimensions = DIMENSIONS.TEN_BY_TEN.VALUE;
const dimensions = writable(defaultDimensions);

export default dimensions;
