import express from 'express';
import morgan from 'morgan';
import * as sapper from '@sapper/server';
import api from './api';

const {PORT} = process.env;
const app = express();

app.use(express.static('static'));
app.use(morgan('tiny'));
app.use('/api', api);
app.use(sapper.middleware());
app.listen(PORT);
