import {isNode} from 'browser-or-node';

function send({method, path, data, token}) {
  const fetch = isNode ? require('node-fetch').default : window.fetch;

  const opts = {method, headers: {}};

  if (data) {
    opts.headers['Content-Type'] = 'application/json';
    opts.body = JSON.stringify(data);
  }

  if (token) {
    opts.headers['Authorization'] = `Token ${token}`;
  }

  return fetch(path, opts);
}

export function get(path, token) {
  return send({method: 'GET', path, token});
}

export function del(path, token) {
  return send({method: 'DELETE', path, token});
}

export function post(path, data, token) {
  return send({method: 'POST', path, data, token});
}

export function put(path, data, token) {
  return send({method: 'PUT', path, data, token});
}