import config from 'config';
const AWS = require('aws-sdk');

const dbConfig = {
  apiVersion: '2012-08-10',
  region: 'us-east-1',
};

AWS.config.update({
  maxRetries: 1,
  httpOptions: {
    timeout: 6000,
    connectTimeout: 6000,
  }
});

if (config.has('DatabaseUri')) {
  dbConfig.endpoint = new AWS.Endpoint(config.get('DatabaseUri'));
}

export const TableName = config.get('TableName');

export default function getDb() {
  return new AWS.DynamoDB.DocumentClient(dbConfig);
};
