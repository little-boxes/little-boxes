import {EventEmitter} from 'events';

/**
 * Utility that subscribes to and provides an API for
 * testing asynchronous derived Svelte stores.
 */
class TestSubscription {

  /**
   * @param  {object} store - a svelte store
   */
  constructor(store) {
    // Initial value of the store while first fetch is pending.
    this.initialValue = undefined;
    // Latest value of the store [except first fetch].
    this.value = undefined;
    // Emits events when updates occur
    this.emitter = new EventEmitter();
    // Boolean indicating whether an update has occured since last isUpdated() call
    this.hasUpdate = false;

    let isInitialized;

    this.unsubscribe = store.subscribe(val => {
      if (!isInitialized) {
        this.initialValue = val;
        isInitialized = true;
      } else {
        this.hasUpdate = true;
        this.value = val;
        this.emitter.emit('update');
      }
    });
  }

  /**
   * @return {Promise} Resolves when store value is updated.
   */
  isUpdated() {
    return new Promise((resolve) => {
      const finish = () => {
        this.hasUpdate = false;
        resolve();
      };

      if (this.hasUpdate) {
        finish();
      } else {
        this.emitter.once('update', finish);
      }
    });
  }

  /**
   * Unsubscribes from store.
   * @return {void}
   */
  destroy() {
    this.unsubscribe();
  }
}

export default TestSubscription;
