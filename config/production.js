module.exports = {
  // Server Config
  TableName: 'ProdTable',

  // Cypress Config
  cypress: {
    baseUrl: 'https://littleboxes.cloud',
  }
};
