module.exports = {
  // Server Config
  DatabaseUri: 'http://local-database:8000',
  TableName: 'DevelopmentTable',

  // Cypress Config
  cypress: {
    baseUrl: 'http://littleboxes:80',
  }
};