module.exports = {
  // Server Config
  DatabaseUri: 'http://localhost:8000',
  TableName: 'DevelopmentTable',

  // Cypress Config
  cypress: {
    baseUrl: 'http://localhost:3000',
  }
};