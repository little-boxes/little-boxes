module.exports = {
  // Server Config
  TableName: 'StageTable',

  // Cypress Config
  cypress: {
    baseUrl: 'https://stage.littleboxes.cloud',
  }
};
