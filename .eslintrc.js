module.exports = {
  extends: [
    "@caseyvangroll/eslint-config-svelte"
  ],
  plugins: ["jest", "cypress"],
  env: {
    "jest/globals": true,
    "cypress/globals": true
  },
  ignorePatterns: ['node_modules/@sapper/**/*'],
  rules: {
    'no-invalid-this': 0,
    'no-unused-vars': ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": true }],
    'new-cap': 0,
    "space-before-function-paren": ["error", {
      "anonymous": "always",
      "named": "never",
      "asyncArrow": "always"
    }],
    'quote-props': ["error", "as-needed"],
    'quotes': ["error", "single", { "avoidEscape": true }]
  }
};