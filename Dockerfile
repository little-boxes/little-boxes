FROM node:12-alpine

WORKDIR /usr/src/app

EXPOSE 80

ENV PORT=80
ENV CYPRESS_INSTALL_BINARY=0

COPY src src
COPY static static
COPY config config
COPY package*.json *.config.js ./

RUN npm install
RUN npm run build

CMD ["npm", "start"]
