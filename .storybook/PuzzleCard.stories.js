import PuzzleCard from '../src/routes/browse/_components/PuzzleCard.svelte';

export default {title: 'PuzzleCard'};

export const fresh = () => ({
  Component: PuzzleCard,
  props: {
    puzzleId: 'puzzle_234',
    title: 'This Year',
    likedCt: 0,
    coloringCt: 0,
    solvedCt: 0,
    artistDisplayName: 'John Darnielle',
  },
});

export const bigStats = () => ({
  Component: PuzzleCard,
  props: {
    puzzleId: 'puzzle_123',
    title: 'Peacocks in the Video Rain, Second Edition with Extra Strength',
    likedCt: 4000000,
    coloringCt: 2000000,
    solvedCt: 17000000,
    artistDisplayName: 'John Vanderslice Esquire the Third, Son of Michaelangelo and Heir to the Throne of Narnia',
  },
});
